﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Runtime.CompilerServices;

namespace AffineTransformations
{
    class AffineMatrix
    {
        public float[,] items;

        public AffineMatrix()
        {
            this.items = new float[3, 3];
        }
        
        public AffineMatrix(float[,] items)
        {
            this.items = items;
        }

        public AffineMatrix(params float[] items)
        {
            this.items = new float[3, 3];
            for (int i = 0; i < 9; i++)
                this.items[i / 3, i % 3] = items[i];
        }

        public float[] Multiply(params float[] vector)
        {
            // vector * this - умножение слева
            float[] result = new float[3];
            for (int n = 0; n < 3; n++)
                for (int m = 0; m < 3; m++)
                    result[n] += vector[m] * this.items[m, n];
            return result;
        }

        public AffineMatrix Multiply(AffineMatrix matrix)
        {
            // this * matrix - умножение справа
            float[,] result = new float[3, 3];
            for (int m = 0; m < 3; m++)
                for (int n = 0; n < 3; n++)
                    for (int t = 0; t < 3; t++)
                        result[m, n] += this.items[m, t] * matrix.items[t, n];
            this.items = result;
            return this;
        }

        public AffineMatrix Translate(float dx, float dy)
        {
            return this.Multiply(AffineMatrices.Translate(dx, dy));
        }

        public AffineMatrix Rotate(float angle)
        {
            return this.Multiply(AffineMatrices.Rotate(angle));
        }

        public AffineMatrix Scale(float kx, float ky)
        {
            return this.Multiply(AffineMatrices.Scale(kx, ky));
        }
    }

    static class AffineMatrices
    {
        public static AffineMatrix Translate(float dx, float dy)
        {
            return new AffineMatrix(
                1, 0, 0,
                0, 1, 0,
                -dx, -dy, 1);
        }

        public static AffineMatrix Rotate(double angle)
        {
            float angleRadians = (float)(angle * Math.PI / 180);
            float sin = (float)Math.Sin(angleRadians);
            float cos = (float)Math.Cos(angleRadians);
            return new AffineMatrix(
                cos, sin, 0,
                -sin, cos, 0,
                0, 0, 1);
        }

        public static AffineMatrix Scale(float kx, float ky)
        {
            return new AffineMatrix(
                1/kx, 0, 0,
                0, 1/ky, 0,
                0, 0, 1);
        }
    }
    
    static class IPrimitiveExtension
    {
        public static IPrimitive Apply(this IPrimitive primitive, AffineMatrix matrix)
        {
            if (primitive is Point)
            {
                Point point = primitive as Point;
                float[] newPosition = matrix.Multiply(point.x, point.y, 1);
                point.x = newPosition[0];
                point.y = newPosition[1];
            }
            else if (primitive is Segment)
            {
                Segment segment = primitive as Segment;
                Apply(segment.point1, matrix);
                Apply(segment.point2, matrix);
            }
            else if (primitive is Polygon)
            {
                Polygon polygon = primitive as Polygon;
                foreach (Segment segment in polygon.segments)
                    Apply(segment, matrix);
            }
            return primitive;
        }
    }

    static class Lines
    {
        // Проверка пересечения
        static public bool isIntersection(Point A, Point B, Point C, Point D, Point P)
        {
            return ((A.x <= P.x && B.x >= P.x && C.x <= P.x && D.x >= P.x)
                 || (A.y <= P.y && B.y >= P.y && C.y <= P.y && D.y >= P.y)
                 || (B.y <= P.y && A.y >= P.y && C.y <= P.y && D.y >= P.y)
                 || (A.y <= P.y && B.y >= P.y && D.y <= P.y && C.y >= P.y))
                 && ((B.x - A.x) / (B.y - A.y)) != ((D.x - C.x) / (D.y - C.y));
        }

        // Возвращает точку пересечения двух отрезков (метод из лекций)
        static public Point Intersection(Point A, Point B, Point C, Point D)
        {
            Vector a = new Vector(A.x, A.y);
            Vector b = new Vector(B.x, B.y);
            Vector c = new Vector(C.x, C.y);
            Vector d = new Vector(D.x, D.y);
            Vector cd = new Vector(D.x - C.x, D.y - C.y);
            Vector n = new Vector(-1 * cd.Y, cd.X);     // нормаль к вектору cd

            double t = -1 * (n * (a - c)) / (n * (b - a));
            Vector pt = a + t * (b - a);
            return new Point((float)pt.X, (float)pt.Y);
        }

        // другой метод
        static public Point SimpleIntersection(Point A, Point B, Point C, Point D)
        {
            float Ax = A.x, Ay = A.y;
            float ABx = B.x - A.x, ABy = B.y - A.y;

            float Cx = C.x, Cy = C.y;
            float CDx = D.x - C.x, CDy = D.y - C.y;

            float x = (Ax * ABy * CDx - Cx * CDy * ABx - Ay * ABx * CDx + Cy * ABx * CDx) /
                (ABy * CDx - CDy * ABx);
            float y = (Ay * ABx * CDy - Cy * CDx * ABy - Ax * ABy * CDy + Cx * ABy * CDy) /
                (ABx * CDy - CDx * ABy);

            return new Point(x, y);
        }
    }

    static class Points
    {
        public enum PointSegmentRelation
        {
            Lies,
            Left,
            Right
        }
        
        static public PointSegmentRelation PointSegmentPosition(this Segment segment, Point point)
        {
            AffineMatrix matrix = AffineMatrices.Translate(segment.point1.x, segment.point1.y);
            point = new Point(point.x, point.y);
            segment = new Segment(segment.point1.x, segment.point1.y, segment.point2.x, segment.point2.y);
            point.Apply(matrix);
            segment.Apply(matrix);
            float f = point.y * segment.point2.x - point.x * segment.point2.y;
            if (f > 0)
                return PointSegmentRelation.Right;
            else if (f < 0)
                return PointSegmentRelation.Left;
            else
                return PointSegmentRelation.Lies;
        }

        static public PointSegmentRelation PointSegmentPosition(this Point point, Segment segment)
        {
            return segment.PointSegmentPosition(point);
        }
        
        public static Segment Reverse(this Segment segment)
        {
            return new Segment(segment.point2, segment.point1);
        }
        
        public static Polygon Reverse(this Polygon polygon)
        {
            LinkedList<Segment> reversedSegments = new LinkedList<Segment>();
            LinkedListNode<Segment> node = polygon.segments.Last;
            while (node != null)
            {
                reversedSegments.AddLast(node.Value.Reverse());
                node = node.Previous;
            }
            return new Polygon(reversedSegments);
        }
        
        static public Polygon ForceClockwise(this Polygon polygon)
        {
            LinkedList<Segment> segments = new LinkedList<Segment>();
            foreach (Segment segment in polygon.segments)
                segments.AddLast(new Segment(segment.point1.x, segment.point1.y,
                    segment.point2.x, segment.point2.y));
            polygon = new Polygon(segments);
            LinkedListNode<Segment> node = polygon.segments.First;
            int lefts = 0;
            int rights = 0;
            while (node != null)
            {
                Segment currentSegment = node.Value;
                Segment nextSegment = (node.Next ?? node.List.First).Value;
                Point nextPoint = nextSegment.point2;
                switch (PointSegmentPosition(currentSegment, nextPoint))
                {
                    case PointSegmentRelation.Left:
                        lefts++;
                        break;
                    case PointSegmentRelation.Right:
                        rights++;
                        break;
                }
                node = node.Next;
            }
            if (lefts > rights)
            {
                polygon = polygon.Reverse();
            }
            return polygon;
        }
        
        static public bool PointInConvexPolygon(this Point point, Polygon polygon)
        {
            polygon = polygon.ForceClockwise();
            foreach (Segment segment in polygon.segments)
                if (segment.PointSegmentPosition(point) == PointSegmentRelation.Left)
                    return false;
            return true;
        }

        static public bool PointInConvexPolygon(this Polygon polygon, Point point)
        {
            return point.PointInConvexPolygon(polygon);
        }

        static public bool PointInPolygon(this Point point, Polygon polygon)
        {
            float anglesSum = 0;
            foreach (Segment segment in polygon.segments)
            {
                Point point1 = new Point(segment.point1.x, segment.point1.y);
                Point point2 = new Point(segment.point2.x, segment.point2.y);
                point1.Apply(AffineMatrices.Translate(point.x, point.y));
                point2.Apply(AffineMatrices.Translate(point.x, point.y));
                float dot = point1.x * point2.x + point1.y * point2.y;
                float cross = point1.x * point2.y - point1.y * point2.x;
                float length1 = (float)Math.Sqrt(point1.x * point1.x + point1.y * point1.y);
                float length2 = (float) Math.Sqrt(point2.x * point2.x + point2.y * point2.y);
                float angleCos = dot / (length1 * length2);
                float angleSin = cross / (length1 * length2);
                float angle = (float)(Math.Acos(angleCos) * 180 / Math.PI * Math.Sign(angleSin));
                anglesSum += angle;
            }
            return Math.Abs(Math.Abs(anglesSum) - 360) <= 0.1;
        }

        static public bool IsConvex(this Polygon polygon)
        {
            polygon = polygon.ForceClockwise();
            LinkedListNode<Segment> node = polygon.segments.First;
            int lefts = 0;
            while (node != null)
            {
                Segment currentSegment = node.Value;
                Segment nextSegment = (node.Next ?? node.List.First).Value;
                Point nextPoint = nextSegment.point2;
                if (PointSegmentPosition(currentSegment, nextPoint) == PointSegmentRelation.Left)
                    lefts++;
                node = node.Next;
            }
            if (lefts != 0)
                return false;
            return true;
        }
    }
}