﻿﻿using System.Windows.Forms;

namespace AffineTransformations
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.scaleButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.scaleYTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.scaleXTextBox = new System.Windows.Forms.TextBox();
            this.primitivesListBox = new System.Windows.Forms.ListBox();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.clearSceneButton = new System.Windows.Forms.Button();
            this.addPolygonButton = new System.Windows.Forms.Button();
            this.addSegmentButton = new System.Windows.Forms.Button();
            this.addPointButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.translateButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.translateYTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.translateXTextBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rotateButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.rotateAngleTextBox = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rotateRightButton = new System.Windows.Forms.Button();
            this.rotateLeftButton = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.addIntersectionPointButton = new System.Windows.Forms.Button();
            this.statusLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox4, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.primitivesListBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.groupBox3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.groupBox5, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.groupBox6, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.statusLabel, 0, 7);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 101F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 101F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(843, 617);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.scaleButton);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.scaleYTextBox);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.scaleXTextBox);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Enabled = false;
            this.groupBox4.Location = new System.Drawing.Point(717, 417);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(123, 95);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Масштабирование";
            // 
            // scaleButton
            // 
            this.scaleButton.Location = new System.Drawing.Point(5, 69);
            this.scaleButton.Name = "scaleButton";
            this.scaleButton.Size = new System.Drawing.Size(113, 22);
            this.scaleButton.TabIndex = 4;
            this.scaleButton.Text = "Масштабировать";
            this.scaleButton.UseVisualStyleBackColor = true;
            this.scaleButton.Click += new System.EventHandler(this.scaleButton_Click);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(5, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Y";
            // 
            // scaleYTextBox
            // 
            this.scaleYTextBox.Location = new System.Drawing.Point(23, 44);
            this.scaleYTextBox.Name = "scaleYTextBox";
            this.scaleYTextBox.Size = new System.Drawing.Size(96, 20);
            this.scaleYTextBox.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(5, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = "X";
            // 
            // scaleXTextBox
            // 
            this.scaleXTextBox.Location = new System.Drawing.Point(23, 19);
            this.scaleXTextBox.Name = "scaleXTextBox";
            this.scaleXTextBox.Size = new System.Drawing.Size(96, 20);
            this.scaleXTextBox.TabIndex = 0;
            // 
            // primitivesListBox
            // 
            this.primitivesListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.primitivesListBox.FormattingEnabled = true;
            this.primitivesListBox.Location = new System.Drawing.Point(717, 3);
            this.primitivesListBox.Name = "primitivesListBox";
            this.primitivesListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.primitivesListBox.Size = new System.Drawing.Size(123, 93);
            this.primitivesListBox.TabIndex = 0;
            this.primitivesListBox.SelectedIndexChanged += new System.EventHandler(this.primitivesListBox_SelectedIndexChanged);
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.White;
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(3, 3);
            this.pictureBox.Name = "pictureBox";
            this.tableLayoutPanel1.SetRowSpan(this.pictureBox, 7);
            this.pictureBox.Size = new System.Drawing.Size(708, 594);
            this.pictureBox.TabIndex = 3;
            this.pictureBox.TabStop = false;
            this.pictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PictureBoxMouseClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.clearSceneButton);
            this.groupBox1.Controls.Add(this.addPolygonButton);
            this.groupBox1.Controls.Add(this.addSegmentButton);
            this.groupBox1.Controls.Add(this.addPointButton);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(717, 102);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(123, 126);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Сцена";
            // 
            // clearSceneButton
            // 
            this.clearSceneButton.Location = new System.Drawing.Point(5, 100);
            this.clearSceneButton.Name = "clearSceneButton";
            this.clearSceneButton.Size = new System.Drawing.Size(113, 22);
            this.clearSceneButton.TabIndex = 3;
            this.clearSceneButton.Text = "Очистить сцену";
            this.clearSceneButton.UseVisualStyleBackColor = true;
            this.clearSceneButton.Click += new System.EventHandler(this.ClearClick);
            // 
            // addPolygonButton
            // 
            this.addPolygonButton.Location = new System.Drawing.Point(5, 73);
            this.addPolygonButton.Name = "addPolygonButton";
            this.addPolygonButton.Size = new System.Drawing.Size(113, 22);
            this.addPolygonButton.TabIndex = 2;
            this.addPolygonButton.Text = "Добавить полигон";
            this.addPolygonButton.UseVisualStyleBackColor = true;
            this.addPolygonButton.Click += new System.EventHandler(this.AddPolygonClick);
            // 
            // addSegmentButton
            // 
            this.addSegmentButton.Location = new System.Drawing.Point(5, 46);
            this.addSegmentButton.Name = "addSegmentButton";
            this.addSegmentButton.Size = new System.Drawing.Size(113, 22);
            this.addSegmentButton.TabIndex = 1;
            this.addSegmentButton.Text = "Добавить отрезок";
            this.addSegmentButton.UseVisualStyleBackColor = true;
            this.addSegmentButton.Click += new System.EventHandler(this.AddSegmentClick);
            // 
            // addPointButton
            // 
            this.addPointButton.Location = new System.Drawing.Point(5, 19);
            this.addPointButton.Name = "addPointButton";
            this.addPointButton.Size = new System.Drawing.Size(113, 22);
            this.addPointButton.TabIndex = 0;
            this.addPointButton.Text = "Добавить точку";
            this.addPointButton.UseVisualStyleBackColor = true;
            this.addPointButton.Click += new System.EventHandler(this.AddPointClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.translateButton);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.translateYTextBox);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.translateXTextBox);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Enabled = false;
            this.groupBox2.Location = new System.Drawing.Point(717, 234);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(123, 95);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Смещение";
            // 
            // translateButton
            // 
            this.translateButton.Location = new System.Drawing.Point(5, 69);
            this.translateButton.Name = "translateButton";
            this.translateButton.Size = new System.Drawing.Size(113, 22);
            this.translateButton.TabIndex = 4;
            this.translateButton.Text = "Переместить";
            this.translateButton.UseVisualStyleBackColor = true;
            this.translateButton.Click += new System.EventHandler(this.translateButton_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(5, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Y";
            // 
            // translateYTextBox
            // 
            this.translateYTextBox.Location = new System.Drawing.Point(23, 44);
            this.translateYTextBox.Name = "translateYTextBox";
            this.translateYTextBox.Size = new System.Drawing.Size(96, 20);
            this.translateYTextBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(5, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "X";
            // 
            // translateXTextBox
            // 
            this.translateXTextBox.Location = new System.Drawing.Point(23, 19);
            this.translateXTextBox.Name = "translateXTextBox";
            this.translateXTextBox.Size = new System.Drawing.Size(96, 20);
            this.translateXTextBox.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rotateButton);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.rotateAngleTextBox);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Enabled = false;
            this.groupBox3.Location = new System.Drawing.Point(717, 335);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(123, 76);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Поворот";
            // 
            // rotateButton
            // 
            this.rotateButton.Location = new System.Drawing.Point(5, 44);
            this.rotateButton.Name = "rotateButton";
            this.rotateButton.Size = new System.Drawing.Size(113, 22);
            this.rotateButton.TabIndex = 7;
            this.rotateButton.Text = "Повернуть";
            this.rotateButton.UseVisualStyleBackColor = true;
            this.rotateButton.Click += new System.EventHandler(this.rotateButton_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(5, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Угол";
            // 
            // rotateAngleTextBox
            // 
            this.rotateAngleTextBox.Location = new System.Drawing.Point(45, 19);
            this.rotateAngleTextBox.Name = "rotateAngleTextBox";
            this.rotateAngleTextBox.Size = new System.Drawing.Size(73, 20);
            this.rotateAngleTextBox.TabIndex = 5;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rotateRightButton);
            this.groupBox5.Controls.Add(this.rotateLeftButton);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Enabled = false;
            this.groupBox5.Location = new System.Drawing.Point(717, 518);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(123, 45);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Поворот (отрезок)";
            // 
            // rotateRightButton
            // 
            this.rotateRightButton.Location = new System.Drawing.Point(65, 19);
            this.rotateRightButton.Name = "rotateRightButton";
            this.rotateRightButton.Size = new System.Drawing.Size(53, 22);
            this.rotateRightButton.TabIndex = 1;
            this.rotateRightButton.Text = "-90°";
            this.rotateRightButton.UseVisualStyleBackColor = true;
            this.rotateRightButton.Click += new System.EventHandler(this.rotateMinus90_Click);
            // 
            // rotateLeftButton
            // 
            this.rotateLeftButton.Location = new System.Drawing.Point(5, 19);
            this.rotateLeftButton.Name = "rotateLeftButton";
            this.rotateLeftButton.Size = new System.Drawing.Size(53, 22);
            this.rotateLeftButton.TabIndex = 0;
            this.rotateLeftButton.Text = "90°";
            this.rotateLeftButton.UseVisualStyleBackColor = true;
            this.rotateLeftButton.Click += new System.EventHandler(this.rotate90_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.addIntersectionPointButton);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Enabled = false;
            this.groupBox6.Location = new System.Drawing.Point(717, 569);
            this.groupBox6.Name = "groupBox6";
            this.tableLayoutPanel1.SetRowSpan(this.groupBox6, 2);
            this.groupBox6.Size = new System.Drawing.Size(123, 45);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Точка пересечения";
            // 
            // addIntersectionPointButton
            // 
            this.addIntersectionPointButton.Location = new System.Drawing.Point(5, 19);
            this.addIntersectionPointButton.Name = "addIntersectionPointButton";
            this.addIntersectionPointButton.Size = new System.Drawing.Size(113, 22);
            this.addIntersectionPointButton.TabIndex = 0;
            this.addIntersectionPointButton.Text = "Добавить точку";
            this.addIntersectionPointButton.UseVisualStyleBackColor = true;
            this.addIntersectionPointButton.Click += new System.EventHandler(this.AddIntersectionPoint_Click);
            // 
            // statusLabel
            // 
            this.statusLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statusLabel.Location = new System.Drawing.Point(3, 600);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(708, 17);
            this.statusLabel.TabIndex = 9;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 617);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MainForm";
            this.Text = "Афинные преобразования";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ListBox primitivesListBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.TextBox translateYTextBox;
        private System.Windows.Forms.TextBox translateXTextBox;
        private System.Windows.Forms.Button clearSceneButton;
        private System.Windows.Forms.Button addPolygonButton;
        private System.Windows.Forms.Button addSegmentButton;
        private System.Windows.Forms.Button addPointButton;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button addIntersectionPointButton;
        private System.Windows.Forms.Button rotateRightButton;
        private System.Windows.Forms.Button rotateLeftButton;
        private System.Windows.Forms.Button scaleButton;
        private System.Windows.Forms.TextBox scaleYTextBox;
        private System.Windows.Forms.TextBox scaleXTextBox;
        private System.Windows.Forms.Button rotateButton;
        private System.Windows.Forms.TextBox rotateAngleTextBox;
        private System.Windows.Forms.Button translateButton;
    }
}
