﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace AffineTransformations
{    
    interface IPrimitive
    {
        void Draw(Graphics graphics);

        void DrawSelection(Graphics graphics);
    }

    public class Point : IPrimitive
    {
        public float x;
        public float y;

        public Point() { }

        public Point(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public void Draw(Graphics graphics)
        {
            graphics.FillRectangle(Brushes.Black, x, y, 1, 1);
        }

        public void DrawSelection(Graphics graphics)
        {
            graphics.FillRectangle(Brushes.Red, x, y, 1, 1);
            graphics.DrawEllipse(Pens.Red, x - 10, y - 10, 20, 20);
        }

        public override string ToString()
        {
            return "точка(" + this.x + ";" + this.y + ")";
        }
    }

    public class Segment : IPrimitive
    {
        public Point point1;
        public Point point2;

        public Point Center
        {
            get { return new Point((point1.x + point2.x) / 2, (point1.y + point2.y) / 2); }
        }

        public Segment() { }

        public Segment(Point point1, Point point2)
        {
            this.point1 = point1;
            this.point2 = point2;
        }

        public Segment(float x1, float y1, float x2, float y2)
        {
            this.point1 = new Point(x1, y1);
            this.point2 = new Point(x2, y2);
        }

        public void Draw(Graphics graphics)
        {
            graphics.DrawLine(Pens.Black, point1.x, point1.y, point2.x, point2.y);
        }

        public void DrawSelection(Graphics graphics)
        {
            Pen pen = new Pen(Color.Red, 1);
            pen.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;
            graphics.DrawLine(pen, point1.x, point1.y, point2.x, point2.y);
        }


        public override string ToString()
        {
            return "отрезок(" + Center.x + ";" + Center.y + ")";
        }
    }

    public class Polygon : IPrimitive
    {
        public LinkedList<Segment> segments;

        public Point Center
        {
            get
            {
                return new Point(
                    segments.Select(segment => segment.point1.x).Average(),
                    segments.Select(segment => segment.point1.y).Average());
            }
        }

        public Polygon()
        {
            this.segments = new LinkedList<Segment>();
        }

        public Polygon(IEnumerable<Segment> segments)
        {
            this.segments = new LinkedList<Segment>(segments);
        }

        public void Draw(Graphics graphics)
        {
            foreach (var segment in segments)
                segment.Draw(graphics);
        }

        public void DrawSelection(Graphics graphics)
        {
            foreach (var segment in segments)
                graphics.DrawLine(Pens.Red, segment.point1.x, segment.point1.y, segment.point2.x, segment.point2.y);
        }

        public override string ToString()
        {
            return "полигон(" + Center.x + ";" + Center.y + ")";
        }
    }
}